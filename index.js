const fs = require('fs');
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const maxFileSize = 1000; //size in bytes
var path = require('path');
var port = 3000;
var users = [];
var logFile = 'logs.txt';


server.listen(port, () => {
    console.log('Server listening at port %d', port);
});

// Routing
app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.get('/style.css', function(req, res) {
    res.sendFile(__dirname + '/style.css');
});

app.get('/main.js', function(req, res) {
    res.sendFile(__dirname + '/main.js');
});


function addLog(message) {
    var logTime = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
    fs.appendFile(logFile, '[' + logTime + ']' + message + '\n', function(err) {
        if (err) throw err;
    });
}

function logMessage(message) {

    var fileSize = fs.statSync(logFile).size;

    if (fileSize > maxFileSize) {
        fs.readFile(logFile, 'utf8', function(err, data) {
            if (err) {
                throw err;
            }
            var reducedText = data.split('\n').slice(1).join('\n');
            fs.writeFile(logFile, reducedText, function(err, result) {
                if (err) console.log('error', err);
                addLog(message)
            });
        });

    } else {
        addLog(message);
    }


}

// Chatroom

var numUsers = 0;
io.on('connection', (socket) => {
    var addedUser = false;

    socket.on('new broadcast', (data) => {

        logMessage('[BROADCAST] ' + socket.username + " BROADCASTED MESSAGE: " + data);
        socket.broadcast.emit('new broadcast', {
            username: socket.username,
            message: data
        });
    });

    socket.on('add user', (username) => {

        if (addedUser) return;

        // stores the user data in the users array
        // check if user is existent
        var user = {
            username: username,
            socketId: socket.id
        };
        var userIndex = users.findIndex(x => x.username == username);
        if (userIndex != -1) {
            users[userIndex] = user;
        } else {
            users.push(user);
        }

        socket.username = username;
        ++numUsers;
        addedUser = true;
        logMessage('[JOIN] ' + socket.username + " JOINED CHAT");
        socket.emit('login', {
            numUsers: numUsers
        });
        socket.broadcast.emit('user joined', {
            username: socket.username,
            numUsers: numUsers
        });
    });

    socket.on('typing', () => {
        socket.broadcast.emit('typing', {
            username: socket.username
        });
    });


    socket.on('stop typing', () => {
        socket.broadcast.emit('stop typing', {
            username: socket.username
        });
    });

    socket.on('new pm', (pm) => {
        var recipient = pm.recipient;
        var userIndex = users.findIndex(x => x.username == recipient);
        if (userIndex === -1) return;
        var recipientSocketId = users[userIndex]['socketId'];
        logMessage('[PM] ' + socket.username + " SENT PM TO " + recipient + ' MESSAGE: ' + pm.message);
        io.to(recipientSocketId).emit('new pm', {
            sender: socket.username,
            message: pm.message
        });
    });

    socket.on('pm received', (report) => {
        var recipient = report.sender;
        var userIndex = users.findIndex(x => x.username == recipient);
        if (userIndex === -1) return;
        var recipientSocketId = users[userIndex]['socketId'];
        io.to(recipientSocketId).emit('pm received', {
            receiver: socket.username
        });
    });

    socket.on('disconnect', () => {
        if (addedUser) {
            --numUsers;
            logMessage('[LEFT] ' + socket.username + " LEFT CHAT");
            socket.broadcast.emit('user left', {
                username: socket.username,
                numUsers: numUsers
            });
        }
    });
});
