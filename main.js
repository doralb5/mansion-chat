var FADE_TIME = 150; // ms
var TYPING_TIMER_LENGTH = 400; // ms
var COLORS = [
    'black', 'blue', 'red', 'green',
    'aqua', 'wheat'
];

var $window = $(window);
var $usernameInput = $('.usernameInput');
var $messages = $('.messages');
var $inputMessage = $('.inputMessage');
var $loginPage = $('.login.page');
var $chatPage = $('.chat.page');
var $recipient = $('.inputUser');
var username;
var connected = false;
var typing = false;
var lastTypingTime;
var $currentInput = $usernameInput;
var socket = io();

const printParticipantsMessage = (data) => {
    var message = '';
    if (data.numUsers === 1) {
        message += "there's 1 participant";
    } else {
        message += "there are " + data.numUsers + " participants";
    }
    log(message);
}

// Sets client's username
const setUsername = () => {
    username = cleanInput($usernameInput.val().trim());
    if (username) {
        $loginPage.fadeOut(1000);
        $chatPage.show();
        $loginPage.off('click');
        $currentInput = $inputMessage.focus();
        // Tell the server your username
        socket.emit('add user', username);
    }
}

const sendMessage = () => {
    var message = $inputMessage.val();
    message = cleanInput(message);
    if (message && connected) {
        $inputMessage.val('');
        log('You broacasted a message.');
        addChatMessage({
            username: username,
            message: message
        });
        socket.emit('new broadcast', message);
    }
}

const sendPM = () => {
    var message = $inputMessage.val();
    var recipient = $recipient.val();
    message = cleanInput(message);
    if (message && connected) {
        $inputMessage.val('');
        log('You sent a PM to ' + recipient);
        addChatMessage({
            username: username,
            message: message,
            recipient: recipient
        });
        socket.emit('new pm', {
            recipient: recipient,
            message: message
        });
    }
}

const log = (message, options) => {
    var $el = $('<li>').addClass('log').text(message);
    addMessageElement($el, options);
}


const addChatMessage = (data, pm, options) => {
  
    pm = pm || false;
    options = options || {};

    var $typingMessages = getTypingMessages(data);
    if ($typingMessages.length !== 0) {
        options.fade = false;
        $typingMessages.remove();
    }

    if (pm) {
        var $usernameDiv = $('<span class="username"/>')
            .text('PM from ' + data.sender)
            .css('color', getUsernameColor(data.sender))
            .css('background', 'wheat');
    } else {
        var $usernameDiv = $('<span class="username"/>')
            .text(data.username)
            .css('color', getUsernameColor(data.username));
    }

    var $messageBodyDiv = $('<span class="messageBody">')
        .text(data.message);

    var typingClass = data.typing ? 'typing' : '';
    var $messageDiv = $('<li class="message"/>')
        .data('username', data.username)
        .addClass(typingClass)
        .append($usernameDiv, $messageBodyDiv);

    addMessageElement($messageDiv, options);
}

const addChatTyping = true;

const removeChatTyping = (data) => {
    getTypingMessages(data).fadeOut(() => {
        $(this).remove();
    });
}

const addMessageElement = (el, options) => {
    var $el = $(el);
    if (!options) {
        options = {};
    }
    if (typeof options.fade === 'undefined') {
        options.fade = true;
    }
    if (typeof options.prepend === 'undefined') {
        options.prepend = false;
    }
    if (options.fade) {
        $el.hide().fadeIn(FADE_TIME);
    }
    if (options.prepend) {
        $messages.prepend($el);
    } else {
        $messages.append($el);
    }
    $messages[0].scrollTop = $messages[0].scrollHeight;
}

const cleanInput = (input) => {
    return $('<div/>').text(input).html();
}

const updateTyping = () => {
    if (connected) {
        if (!typing) {
            typing = true;
            socket.emit('typing');
        }
        lastTypingTime = (new Date()).getTime();

        setTimeout(() => {
            var typingTimer = (new Date()).getTime();
            var timeDiff = typingTimer - lastTypingTime;
            if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
                socket.emit('stop typing');
                typing = false;
            }
        }, TYPING_TIMER_LENGTH);
    }
}

const getTypingMessages = (data) => {
    return $('.typing.message').filter(i => {
        return $(this).data('username') === data.username;
    });
}

// Gets the color of a username through our hash function
const getUsernameColor = (username) => {
    var hash = 7;
    for (var i = 0; i < username.length; i++) {
        hash = username.charCodeAt(i) + (hash << 5) - hash;
    }
    var index = Math.abs(hash % COLORS.length);
    return COLORS[index];
}

$window.keydown(event => {
    if (!(event.ctrlKey || event.metaKey || event.altKey)) {}
    if (event.which === 13) {
        if (username) {
            if ($recipient.val() != '') {
                sendPM();
            } else {
                sendMessage();
            }
            socket.emit('stop typing');
            typing = false;
        } else {
            setUsername();
        }
    }
});

$inputMessage.on('input', () => {
    updateTyping();
});

$loginPage.click(() => {
    $currentInput.focus();
});

$inputMessage.click(() => {
    $inputMessage.focus();
});

socket.on('login', (data) => {
    connected = true;
    var message = "Mansion chat test";
    log(message, {
        prepend: true
    });
    printParticipantsMessage(data);
});

socket.on('new broadcast', (data) => {
    addChatMessage(data);
});

socket.on('new pm', (data) => {
    socket.emit('pm received', {sender: data.sender});
    addChatMessage(data, true);
});

socket.on('pm received', (data) => {
  log(data.receiver + ' received your PM', {
      prepend: false
  });
});

socket.on('user joined', (data) => {
    log(data.username + ' joined');
    printParticipantsMessage(data);
});

socket.on('user left', (data) => {
    log(data.username + ' left');
    printParticipantsMessage(data);
    removeChatTyping(data);
});

socket.on('typing', (data) => {
    //addChatTyping(data);
});

socket.on('stop typing', (data) => {
    removeChatTyping(data);
});

socket.on('disconnect', () => {
    log('you have been disconnected');
});

socket.on('reconnect', () => {
    log('you have been reconnected');
    if (username) {
        socket.emit('add user', username);
    }
});

socket.on('reconnect_error', () => {
    log('attempt to reconnect has failed');
});
