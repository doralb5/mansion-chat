# mansion-chat

## Installation Guide

### Clone the repository

```
git clone https://gitlab.com/doralb5/mansion-chat.git
```

### open containig folder

```
cd mansion-chat
```

### Install the dependency packages

```
npm install
```

### Run the app

```
node index.js
```